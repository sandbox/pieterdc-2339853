<?php
/**
 * @file
 * Graceful menu database cache base class.
 */

/**
 * Overrides the Drupal database cache, to (un)flag cache as stale.
 */
class GracefulMenuCacheBase extends DrupalDatabaseCache {}
