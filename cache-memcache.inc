<?php
/**
 * @file
 * Graceful menu database cache base class.
 */

/**
 * Overrides the Drupal memcache cache, to (un)flag cache as stale.
 */
class GracefulMenuCacheBase extends MemCacheDrupal {}
