Graceful Menu Cache flags menu caches as stale instead of clearing them. At
cron run, it generates new caches and then replaces the old caches. Doing so,
users only have to wait for the cache to build, the first time it gets loaded.
This gives a major performance boost to Drupal installations with a lot of menu
items.

The disadvantage is that it takes longer for anonymous users to see menu
changes. It takes as long as the cron frequency.
