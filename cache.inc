<?php
/**
 * @file
 * Graceful menu database cache implementation.
 */

/**
 * Overrides any other cache handler, to (un)flag cache as stale.
 */
class GracefulMenuCache extends GracefulMenuCacheBase {

  /**
   * Flags cache data as expired. The data will be cleared on the next cron run.
   *
   * @param $cid
   *   If set, the cache ID or an array of cache IDs. Otherwise, all cache
   *   entries that can expire are deleted. The $wildcard argument will be
   *   ignored if set to NULL.
   * @param $wildcard
   *   If TRUE, the $cid argument must contain a string value and cache IDs
   *   starting with $cid are deleted in addition to the exact cache ID
   *   specified by $cid. If $wildcard is TRUE and $cid is '*', the entire
   *   cache is emptied.
   */
  public function clear($cid = NULL, $wildcard = FALSE) {
    // Flag as stale instead of actually clearing the cache.
    // Refreshing cache happens on cron.
    $queue = DrupalQueue::get('graceful_menu_cache');

    // @codingStandardsIgnoreStart
    /* @var DrupalQueueInterface $queue */
    // @codingStandardsIgnoreEnd
    $queue->createItem(array('cid' => $cid, 'wildcard' => $wildcard));
  }

  /**
   * Call this function to get an instant clear, using the parent cache handler.
   *
   * This method does not wait for cron to clear the expired data.
   *
   * @param string $cid
   *   Cache id.
   * @param bool $wildcard
   *   Wildcard.
   *
   * @throws Exception
   */
  public function instantClear($cid = NULL, $wildcard = FALSE) {
    parent::clear($cid, $wildcard);
  }

}
