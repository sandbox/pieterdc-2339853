<?php

/**
 * @file
 * Hooks provided by the Graceful Menu Cache module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Act on menu link update after Graceful Menu Cache cleared the core caches.
 *
 * @param array $link
 *   Associative array defining a menu link as passed into menu_link_save().
 * @param array $existing_item
 *   Associative array defining the menu link as it is pre-update.
 *
 * @see hook_menu_link_update()
 */
function hook_graceful_menu_cache_menu_link_update($link, $existing_item) {
  $cache_handler = new GracefulMenuCache('cache_menu');
  // Clear related menu structure panes.
  $cid = 'links:' . $link['menu_name'] . ':menu-structure:';
  $cache_handler->instantClear($cid, TRUE);
}

/**
 * Rebuild a menu cache, Graceful Menu Cache could not rebuild itself.
 *
 * @param string $cid
 *   String representing the cache id.
 *
 * @return mixed
 *   The data to cache. Return FALSE if rebuilding did not succeed.
 *
 * @see graceful_menu_cache_rebuild_cids()
 */
function hook_graceful_menu_cache_rebuild($cid) {
  $cid_parts = explode(':', $cid);
  // Check if the cache id contains ':menu-structure:'.
  if ($cid_parts[0] == 'links' && $cid_parts[2] == 'menu-structure') {
    $menu_name = $cid_parts[1];
    $data = my_custom_module_get_menu_data($menu_name);

    return array($cid => $data);
  }

  return FALSE;
}

/**
 * @} End of "addtogroup hooks".
 */
